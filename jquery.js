$(document).ready(function () {
    $("#image1").hover(function(){
        $("#p1").css("display", "block");
        $("#image1").css("opacity", "1");
        $("#image2").css("opacity", "0.5");
        $("#image3").css("opacity", "0.5");
    }
        ,function () {
            $("#p1").css("display", "none");
            $("#image2").css("opacity", "1");
            $("#image3").css("opacity", "1");
        });

      $("#image2").hover(function(){
        $("#p2").css("display", "block");
        $("#image2").css("opacity", "1");
        $("#image1").css("opacity", "0.5");
        $("#image3").css("opacity", "0.5");
      }
      ,function () {
        $("#p2").css("display", "none");
        $("#image1").css("opacity", "1");
        $("#image3").css("opacity", "1");
    });


      $("#image3").hover(function(){
        $("#p3").css("display", "block");
        $("#image3").css("opacity", "1");
        $("#image1").css("opacity", "0.5");
        $("#image2").css("opacity", "0.5");
      }
      ,function () {
        $("#p3").css("display", "none");
        $("#image1").css("opacity", "1");
        $("#image2").css("opacity", "1");
    });
});